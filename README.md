# Chaotic AUR Mirrors Status

A simple project to check the current status of the [Chaotic AUR](https://aur.chaotic.cx/)'s mirrors.

## Build instructions:

```Bash
$ meson build
$ ninja -C build
$ build/chaotic-aur-mirrors-status
```
