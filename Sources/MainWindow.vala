using Soup;

public class ChaoticAurMirrorsStatus.MainWindow : Gtk.ApplicationWindow {
    
    public static Gtk.Box boxServerList;
    
    private static async List<string> read_lines_async (InputStream stream) throws IOError {
	    DataInputStream data_stream = new DataInputStream (stream);
	    string line;
	    List<string> lines = new List<string>();
	    
	    while ((line = yield data_stream.read_line_async ()) != null) {
		    lines.append(line);
	    }
	
	    return lines;
	}   
	
	public static void refresh_server_list () {
 
        boxServerList.set_halign (Gtk.Align.CENTER);
        while (boxServerList.get_first_child () != null)
            boxServerList.remove (boxServerList.get_first_child ());
        
        Soup.Session session = new Soup.Session ();
	    session.use_thread_context = true;

	// Request a file:
	    try {
		    Soup.Request request = session.request ("https://status.chaotic.cx/mirrors");
		    request.send_async.begin (null, (obj, res) => {
			    // get the content of the response
			    try {
				    InputStream stream = request.send_async.end (res);
				    read_lines_async.begin (stream, (obj, res) => {
					    try {
                            var result = read_lines_async.end(res);
					        foreach (string line in result) {
					            var boxServer = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
                                var labelServerName = new Gtk.Label (line.split(" ")[0]);
                                string serverStatus = line.split(" ")[2];
                                var labelServerStatus = new Gtk.Label (serverStatus);
                                if (serverStatus != "Healthy") {
                                    labelServerStatus.get_style_context().add_class("error_text");
                                } else {
                                    labelServerStatus.get_style_context().add_class("ok_text");
                                }
                                
                                boxServer.append (labelServerName);
                                boxServer.append (labelServerStatus);
        
                                boxServerList.append (boxServer);
					        }
					    }
					    catch (Error e) {
					        boxServerList.append (new Gtk.Label ("Error: ".concat(e.message)));
					    }
				    });
			    } catch (Error e) {
				    boxServerList.append (new Gtk.Label ("Error: ".concat(e.message)));
			    }
		    });
	    } catch (Error e) {
		    boxServerList.append (new Gtk.Label ("Error: ".concat(e.message)));
	     }
	}

    // GLib.ListStore clocks_list_store;
    public MainWindow (Gtk.Application app) {
        Object (application: app);

        this.default_height = 600;
        this.default_width = 400;
        this.title = "Chaotic AUR - Mirrors Status";
        
        var display = this.get_display ();
        var css_provider = new Gtk.CssProvider();
        string path = "style.css";
        // test if the css file exist
        if (FileUtils.test (path, FileTest.EXISTS))
        {
            try {
                css_provider.load_from_path(path);
                Gtk.StyleContext.add_provider_for_display(display, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
            } catch (Error e) {
                error ("Cannot load CSS stylesheet: %s", e.message);
            }
        }
          
        var header = new Gtk.HeaderBar ();
        var btnRefresh = new Gtk.Button.with_label ("Refresh");
        btnRefresh.clicked.connect (refresh_server_list);
        
        header.show_title_buttons = true;
        header.pack_start (btnRefresh);
        this.set_titlebar (header);

// cdn-mirror 1632171338 Healthy
        
        boxServerList = new Gtk.Box (Gtk.Orientation.VERTICAL, 4);
        boxServerList.set_homogeneous (true);

        this.child = boxServerList;
        
        refresh_server_list ();
    }
       
}
